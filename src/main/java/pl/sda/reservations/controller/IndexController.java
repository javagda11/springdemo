package pl.sda.reservations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.reservations.model.MessageObject;
import pl.sda.reservations.model.kwadratowe.QuadraticOne;
import pl.sda.reservations.model.kwadratowe.QuadraticTwo;
import pl.sda.reservations.model.kwadratowe.QuadraticZero;
import pl.sda.reservations.service.MessageService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IndexController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public ResponseEntity index() {
        return ResponseEntity.ok(new MessageObject("Hello!"));
    }

    @RequestMapping(path = "/count", method = RequestMethod.GET)
    public ResponseEntity count() {
        return ResponseEntity.ok(messageService.getCounter());
    }

    @GetMapping(path = "/parametrized")
    public ResponseEntity parametrized(@RequestParam(name = "who") String who,
                                       @RequestParam(name = "where", required = false) String where) {

        // ?who=Paweł&where=here!

        return ResponseEntity.ok(new MessageObject("Hello " + who + "!"));
    }

    @GetMapping(path = "/kwadratowe")
    public ResponseEntity kwadratowe(@RequestParam(name = "a") int a,
                                     @RequestParam(name = "b") int b,
                                     @RequestParam(name = "c") int c) {

        double delta = b * b - 4 * a * c;
        if (delta > 0) {
            //
            double x1 = (-b - Math.sqrt(delta)) / (2 * a);
            double x2 = (-b + Math.sqrt(delta)) / (2 * a);
//
//            return ResponseEntity.ok(
//                    new MessageObject("x1: " + x1 + " x2: " + x2 + " delta: " + delta));
            return ResponseEntity.ok(new QuadraticTwo(x1, x2, delta));
        } else if (delta == 0) {
            double x0 = (-b) / (2 * a);

//            return ResponseEntity.ok(
//                    new MessageObject("x0: " + x0 + " delta: " + delta));
            return ResponseEntity.ok(new QuadraticOne(x0, delta));
        } else {
            return ResponseEntity.ok(new QuadraticZero(delta));
//            return ResponseEntity.ok(
//                    new MessageObject(" delta: " + delta));
        }
    }

    // złożoność jest bardzo duża więc działa MEEEEGA WOLNO!
    @GetMapping(path = "/fibo")
    public ResponseEntity fibo(@RequestParam(name = "ile") int howMany) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            list.add(fibonacci(i));
        }

        return ResponseEntity.ok(list);
    }


    private int fibonacci(int poziom) {
        if (poziom == 0) return 0;
        if (poziom == 1) return 1;
        return fibonacci(poziom - 1) + fibonacci(poziom - 2);
    }


}
