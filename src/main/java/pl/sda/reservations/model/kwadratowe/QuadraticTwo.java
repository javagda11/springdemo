package pl.sda.reservations.model.kwadratowe;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuadraticTwo {
    private double x1;
    private double x2;
    private double delta;
}
