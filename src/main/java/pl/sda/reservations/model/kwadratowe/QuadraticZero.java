package pl.sda.reservations.model.kwadratowe;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuadraticZero {
    private double delta;
}
